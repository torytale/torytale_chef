node[:deploy].each do |application, deploy|
  content_root = "#{deploy[:deploy_to]}/current/wp-content"
  execute "chmod -R 0775 #{content_root}" do
  end
end